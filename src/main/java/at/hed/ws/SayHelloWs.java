package at.hed.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class SayHelloWs {

    private static final String SALUTATION = "Hello";

    @WebMethod
    public String getGreeting( String name ) {
        return SALUTATION + " " + name;
    }
}
