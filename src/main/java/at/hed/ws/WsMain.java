package at.hed.ws;

import javax.xml.ws.Endpoint;

public class WsMain {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8090/wsServerExample", new SayHelloWs());
    }
}
